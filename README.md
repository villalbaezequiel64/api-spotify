# Api Spotify. 

### Slim, Micro-Framework

### Requirements: 

- Apache or Nginx
- PHP 7.1 >
- Composer

### Install: 

- composer install

### Run your app :) :

- composer start 
or 
- php -S localhost:8080 -t public public/index.php

### Endpoint:

- Search Albums by Artist: localhost:8080/api/v1/albums/{artist_name}