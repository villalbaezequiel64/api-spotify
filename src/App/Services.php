<?php 

use App\Services\SpotifyService;
use Psr\Container\ContainerInterface;

$container->set('spotify_service',function(ContainerInterface $container){
    return new SpotifyService($container->get('spotify_access'));
});