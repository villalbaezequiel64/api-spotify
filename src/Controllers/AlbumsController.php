<?php
namespace App\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use App\Controllers\BaseController;

class AlbumsController extends BaseController {

    public function getAlbums(Request $request, Response $response, array $arg)
    {
        try {
            $artist = $arg['artist_name'];
            $results= $this->spotifyService->searchAlbums($artist);
    
            $response->getBody()->write(json_encode($results));
            
            return $response->withHeader('Content-Type', 'application/json')->withStatus(201); 

        } catch (\Exception $err) {
            // priority one Slim Application Error, catch this
            return 'ERROR('.$err->getLine().'): '.$err->getMessage();;
        }
    }

}