<?php
namespace App\Controllers;

use Psr\Container\ContainerInterface;

abstract class BaseController {

    protected $container;
    protected $spotifyService;

    public function __construct(ContainerInterface $con)
    {
        $this->container        = $con;
        $this->spotifyService   = $this->container->get('spotify_service');
    }

}