<?php
namespace App\Services;

use GuzzleHttp\Client;

class SpotifyService {

    protected $token;
    private $httpClient;
    
    public function __construct($credentials)
    {
        $this->httpClient   = new Client();
        $this->token        = $this->generateToken($credentials);
    }

    public function generateToken($credentials)
    {
        try {
            $request = $this->httpClient->request('POST', 'https://accounts.spotify.com/api/token', [
                'headers' => [
                    'Content-Type'  => 'application/x-www-form-urlencoded',
                    'Authorization' => 'Basic '.base64_encode($credentials->client_id.':'.$credentials->client_secret),
                ],
                'form_params' => [
                    'grant_type' => 'client_credentials'
                ]
            ]);
    
            $response = json_decode($request->getBody());
            return $response->access_token;

        } catch (\Exception $err) {
            // priority one Slim Application Error, catch this
            return 'ERROR('.$err->getLine().'): '.$err->getMessage();;
        }
    }

    public function searchAlbums($artist)
    {
        try {
            $request = $this->httpClient->request('GET', 'https://api.spotify.com/v1/search', [
                'headers' => [
                    'Content-Type'  => 'application/json',
                    'Accept'        => 'application/json',
                    'Authorization' => 'Bearer '.$this->token
                ],
                'query' => [
                    'q'     => $artist,
                    'type'  => 'album'
                ]
            ]);
    
            $response = json_decode($request->getBody());
            
            return $this->parseAlbums($response);

        } catch (\Exception $err) {
            // priority one Slim Application Error, catch this
            return 'ERROR('.$err->getLine().'): '.$err->getMessage();;
        }
    }

    public function parseAlbums($albums)
    {
        try {
            $items = [];

            foreach($albums->albums->items as $album)
            {
                [
                    "name" => $name,
                    "release_date" => $release_date,
                    "total_tracks" => $total_tracks
                ] = (array) $album;

                $items[] = [
                    "name"      => $name,
                    "released"  => date('d-m-Y', strtotime($release_date)),
                    "tracks"    => $total_tracks,
                    "cover"     => [
                        "height"=> ($album->images{0} ? $album->images{0}->height : ''),
                        "width" => ($album->images{0} ? $album->images{0}->width : ''),
                        "url"   => ($album->images{0} ? $album->images{0}->url : '')
                    ]
                ];
            }

            return $items;

        } catch (\Exception $err) {
            // priority one Slim Application Error, catch this
            return 'ERROR('.$err->getLine().'): '.$err->getMessage();;            
        }
    }

}